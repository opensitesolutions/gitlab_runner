#!/usr/bin/env bash
# Date August 29, 2019
# Get user information and run playbook
# Scrope speed up execution and testing of playbook

# Variables
root_path=/home/cmills/Documents/version_control
inv_src=gitlab/cloud_linode/inventory
inventory=inventory

# Copy inventory from cloud_linode environment
cp $root_path/$inv_src $root_path/bitbucket/gitlab_runner

# Get playbook from user to execute
echo -n "Ener playbook to execute: "
read playbook

# Get token from user
echo "Enter Gitlab user token: "
read gitlab_token

# Execute playbook
ansible-playbook -i $inventory $playbook -e "server_name=all gitlab_token=$gitlab_token"
